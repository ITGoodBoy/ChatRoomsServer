package com.celestialapps.roomchats.impl;

import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.repository.UserRepository;
import com.celestialapps.roomchats.request.UserRequest;
import com.celestialapps.roomchats.response.UserListResponse;
import com.celestialapps.roomchats.response.UserResponse;
import com.celestialapps.roomchats.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey on 15.04.2017.
 */
@Service
public class UserImpl implements UserService {

    private final int USER_LOGIN_MIN_LENGTH = 3;
    private final int USER_LOGIN_MAX_LENGTH = 12;
    private final String USER_LOGIN_NOT_MIXED_ALPHABET_PATTERN = "^([a-zA-Z0-9-_\\\\.]*)|([а-яА-Я0-9-_\\\\.]*)$";

    private final int USER_PASSWORD_MIN_LENGTH = 8;
    private final int USER_PASSWORD_MAX_LENGTH = 16;
    private final String[] EASY_PASSWORDS = {
            "12344321", "12345678", "87654321", "43211234",
            "qwertyui", "iuytrewq", "1qaz1qaz", "zaq1zaq1",
            "1234qwer", "rewq4321"};

    private final String USER_EMAIL_ADDRESS_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+";

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserResponse authorize(String login, String password) {
        User user = userRepository.findByLogin(login);

        if (user == null)
            return new UserResponse(UserResponse.Status.USER_LOGIN_NOT_EXIST);

        if (!user.getPassword().equals(password))
            return new UserResponse(UserResponse.Status.USER_PASSWORD_NOT_EQUALS);

        return new UserResponse(UserResponse.Status.USER_OK, user);
    }

    @Override
    public UserResponse getById(long id) {
        User user = userRepository.findOne(id);

        if (user == null)
            return new UserResponse(UserResponse.Status.USER_ID_NOT_EXIST);

        return new UserResponse(UserResponse.Status.USER_OK, user);
    }

    @Override
    public UserResponse getByLogin(String login) {
        User user = userRepository.findByLogin(login);

        if (user == null)
            return new UserResponse(UserResponse.Status.USER_LOGIN_NOT_EXIST);

        return new UserResponse(UserResponse.Status.USER_OK);
    }

    @Override
    public UserListResponse getAll() {
        List<User> userList = userRepository.findAll();

        if (userList == null)
            userList = new ArrayList<>();

        return new UserListResponse(UserResponse.Status.USER_OK, userList);
    }

    @Override
    public UserResponse create(UserRequest userRequest) {
        if (!userRequest.validate())
            return new UserResponse(UserResponse.Status.USER_EMPTY_FIELDS);

        if (userRepository.findByLogin(userRequest.getLogin()) != null)
            return new UserResponse(UserResponse.Status.USER_LOGIN_EXIST);

        if (userRepository.findByEmail(userRequest.getEmail()) != null)
            return new UserResponse(UserResponse.Status.USER_EMAIL_EXIST);

        User user = new User(userRequest);
        UserResponse userResponse;

        userResponse = loginValidation(user.getLogin());
        if (userResponse != null) return userResponse;

        //userResponse = passwordValidation(user.getPassword());
        //if (userResponse != null) return userResponse;

        userResponse = emailValidation(user.getEmail());
        if (userResponse != null) return userResponse;

        userRepository.save(user);

        return new UserResponse(UserResponse.Status.USER_OK, user);
    }



    @Override
    public UserResponse update(UserRequest userRequest) {
        if (!userRequest.validate())
            return new UserResponse(UserResponse.Status.USER_EMPTY_FIELDS);

        User user = userRepository.findByLogin(userRequest.getLogin());
        if (user == null)
            return new UserResponse(UserResponse.Status.USER_LOGIN_NOT_EXIST);

        if (!user.getPassword().equals(userRequest.getPassword()))
            return new UserResponse(UserResponse.Status.USER_PASSWORD_NOT_EQUALS);

        long userId = user.getId();
        user = new User(userRequest);
        user.setId(userId);

        UserResponse userResponse;

        userResponse = loginValidation(user.getLogin());
        if (userResponse != null) return userResponse;

        //userResponse = passwordValidation(user.getPassword());
        //if (userResponse != null) return userResponse;

        userResponse = emailValidation(user.getEmail());
        if (userResponse != null) return userResponse;

        userRepository.save(user);

        return new UserResponse(UserResponse.Status.USER_OK, user);
    }



    private UserResponse loginValidation(String login) {
        if (login.length() < USER_LOGIN_MIN_LENGTH)
            return new UserResponse(UserResponse.Status.USER_LOGIN_LENGTH_SHORT);

        if (login.length() > USER_LOGIN_MAX_LENGTH)
            return new UserResponse(UserResponse.Status.USER_LOGIN_LENGTH_LONG);

        if (!login.matches(USER_LOGIN_NOT_MIXED_ALPHABET_PATTERN))
            return new UserResponse(UserResponse.Status.USER_LOGIN_MIXED_ALPHABET);

        if (Character.isDigit(login.charAt(0)))
            return new UserResponse(UserResponse.Status.USER_LOGIN_NUMBER_IN_THE_BEGIN);

        return null;
    }

    private UserResponse passwordValidation(String password) {
        if (password.length() < USER_PASSWORD_MIN_LENGTH)
            return new UserResponse(UserResponse.Status.USER_PASSWORD_LENGTH_SHORT);

        if (password.length() > USER_PASSWORD_MAX_LENGTH)
            return new UserResponse(UserResponse.Status.USER_PASSWORD_LENGTH_LONG);

        for (String easyPassword: EASY_PASSWORDS) {
            if (password.equals(easyPassword))
                return new UserResponse(UserResponse.Status.USER_PASSWORD_EASY);
        }

        for (int i = 1; i < password.length(); i++) {
            if (password.charAt(0) != password.charAt(i))
                return null;
        }

       return new UserResponse(UserResponse.Status.USER_PASSWORD_EASY);
    }

    private UserResponse emailValidation(String email) {
        if (email.matches(USER_EMAIL_ADDRESS_PATTERN))
            new UserResponse(UserResponse.Status.USER_EMAIL_INCORRECT);

        return null;
    }
}
