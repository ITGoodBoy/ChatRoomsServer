package com.celestialapps.roomchats.impl;

import com.celestialapps.roomchats.chat.ChatServerHandler;
import com.celestialapps.roomchats.model.Room;
import com.celestialapps.roomchats.repository.RoomRepository;
import com.celestialapps.roomchats.request.RoomRequest;
import com.celestialapps.roomchats.response.RoomListResponse;
import com.celestialapps.roomchats.response.RoomResponse;
import com.celestialapps.roomchats.service.RoomService;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sergey on 15.04.2017.
 */
@Service
public class RoomImpl implements RoomService {

    private final int ROOM_NAME_MIN_LENGTH = 3;
    private final int ROOM_NAME_MAX_LENGTH = 12;
    private final String ROOM_NAME_NOT_MIXED_ALPHABET_PATTERN = "^([a-zA-Z0-9-_\\\\.]*)|([а-яА-Я0-9-_\\\\.]*)$";

    private final int ROOM_PASSWORD_MIN_LENGTH = 4;
    private final int ROOM_PASSWORD_MAX_LENGTH = 10;

    private final int ROOM_MAX_COUNT_USERS = 25;
    private final int ROOM_MIN_MAX_COUNT_USERS = 2;

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public RoomResponse authorize(String name, String password) {
        Room room = roomRepository.findByName(name);

        if (room == null)
            return new RoomResponse(RoomResponse.Status.ROOM_NAME_NOT_EXIST);

        if (!room.isEmptyPassword() && !room.getPassword().equals(password))
            return new RoomResponse(RoomResponse.Status.ROOM_PASSWORD_NOT_EQUALS);

        return new RoomResponse(RoomResponse.Status.ROOM_OK, room);
    }

    @Override
    public RoomResponse getById(long id) {
        Room room = roomRepository.findOne(id);

        if (room == null)
            return new RoomResponse(RoomResponse.Status.ROOM_ID_NOT_EXIST);

        room.setMessageList(null);
        return new RoomResponse(RoomResponse.Status.ROOM_OK, room);
    }

    @Override
    public RoomResponse getByName(String name) {
        Room room = roomRepository.findByName(name);

        if (room == null)
            return new RoomResponse(RoomResponse.Status.ROOM_NAME_NOT_EXIST);

        room.setMessageList(null);
        return new RoomResponse(RoomResponse.Status.ROOM_OK, room);
    }


    @Override
    public RoomListResponse getByMaxCountUsers(int maxCountUsers) {
        List<Room> roomList = roomRepository.findByMaxCountUsers(maxCountUsers);

        if (roomList == null)
            return new RoomListResponse(RoomResponse.Status.ROOM_THIS_MAX_COUNT_USERS_NOT_EXIST);

        for (Room room : roomList) {
            room.setMessageList(null);
        }

        return new RoomListResponse(RoomResponse.Status.ROOM_OK, roomList);
    }

    @Override
    public RoomListResponse getAll() {
        List<Room> roomList = roomRepository.findAll();

        if (roomList == null)
            roomList = new ArrayList<>();

        for (Room room : roomList) {
            room.setMessageList(null);
        }

        return new RoomListResponse(RoomResponse.Status.ROOM_OK, roomList);
    }

    @Override
    public RoomResponse create(RoomRequest roomRequest) {
        if (!roomRequest.validate())
            return new RoomResponse(RoomResponse.Status.ROOM_EMPTY_FIELDS);

        if (roomRepository.findByName(roomRequest.getName()) != null)
            return new RoomListResponse(RoomListResponse.Status.ROOM_NAME_EXIST);

        Room room = new Room(roomRequest);
        RoomResponse roomResponse;

        roomResponse = nameValidation(room.getName());
        if (roomResponse != null) return roomResponse;

        //roomResponse = passwordValidation(room.getPassword());
        //if (roomResponse != null) return roomResponse;

        roomResponse = maxCountUsersValidation(room.getMaxCountUsers());
        if (roomResponse != null) return roomResponse;

        roomRepository.save(room);
        ChatServerHandler.updateChannelGroups();

        return new RoomResponse(RoomResponse.Status.ROOM_OK, room);
    }

    @Override
    public RoomResponse update(RoomRequest roomRequest) {
        if (!roomRequest.validate())
            return new RoomResponse(RoomResponse.Status.ROOM_EMPTY_FIELDS);

        Room room = roomRepository.findByName(roomRequest.getName());
        if (room == null)
            return new RoomResponse(RoomResponse.Status.ROOM_NAME_NOT_EXIST);

        if (!room.getPassword().equals(roomRequest.getPassword()))
            return new RoomResponse(RoomResponse.Status.ROOM_PASSWORD_NOT_EQUALS);

        long roomId = room.getId();
        room = new Room(roomRequest);
        room.setId(roomId);

        RoomResponse roomResponse;

        roomResponse = nameValidation(room.getName());
        if (roomResponse != null) return roomResponse;

        //roomResponse = passwordValidation(room.getPassword());
      //  if (roomResponse != null) return roomResponse;

        roomResponse = maxCountUsersValidation(room.getMaxCountUsers());
        if (roomResponse != null) return roomResponse;

        roomRepository.save(room);
        ChatServerHandler.updateChannelGroups();
        return new RoomResponse(RoomResponse.Status.ROOM_OK, room);
    }

    @Override
    public RoomResponse delete(RoomRequest roomRequest) {
        if (roomRequest.getName() == null
                || roomRequest.getOwner().getLogin() == null
                || roomRequest.getOwner().getPassword() == null)
            return new RoomResponse(RoomResponse.Status.ROOM_EMPTY_FIELDS);

        Room room = roomRepository.findByName(roomRequest.getName());
        if (room == null)
            return new RoomResponse(RoomResponse.Status.ROOM_NAME_NOT_EXIST);

        if (!room.getOwner().getPassword().equals(roomRequest.getOwner().getPassword()))
            return new RoomResponse(RoomResponse.Status.ROOM_OWNER_PASSWORD_NOT_EQUALS);

        roomRepository.delete(room.getId());
        ChatServerHandler.updateChannelGroups();

        return new RoomResponse(RoomResponse.Status.ROOM_OK);
    }

    private RoomResponse nameValidation(String name) {
        if (name.length() < ROOM_NAME_MIN_LENGTH)
            return new RoomResponse(RoomResponse.Status.ROOM_NAME_LENGTH_SHORT);

        if (name.length() > ROOM_NAME_MAX_LENGTH)
            return new RoomResponse(RoomResponse.Status.ROOM_NAME_LENGTH_LONG);

        if (!name.matches(ROOM_NAME_NOT_MIXED_ALPHABET_PATTERN))
            return new RoomResponse(RoomResponse.Status.ROOM_NAME_MIXED_ALPHABET);

        return null;
    }


    private RoomResponse passwordValidation(String password) {
        if (password.length() < ROOM_PASSWORD_MIN_LENGTH)
            return new RoomResponse(RoomResponse.Status.ROOM_PASSWORD_LENGTH_SHORT);

        if (password.length() > ROOM_PASSWORD_MAX_LENGTH)
            return new RoomResponse(RoomResponse.Status.ROOM_PASSWORD_LENGTH_LONG);

        return null;
    }

    private RoomResponse maxCountUsersValidation(int maxCountUsers) {
        if (maxCountUsers > ROOM_MAX_COUNT_USERS)
            return new RoomResponse(RoomResponse.Status.ROOM_MAX_COUNT_USERS_ABOVE_MAXIMUM);

        if (maxCountUsers < ROOM_MIN_MAX_COUNT_USERS)
            return new RoomResponse(RoomResponse.Status.ROOM_MAX_COUNT_USERS_BELOW_MAXIMUM);

        return null;
    }
}
