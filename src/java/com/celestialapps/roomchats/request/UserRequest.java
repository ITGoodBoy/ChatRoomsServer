package com.celestialapps.roomchats.request;

import com.celestialapps.roomchats.model.Room;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Sergey on 30.04.2017.
 */
public class UserRequest {

    @Expose
    private String login;
    @Expose
    private String password;
    @Expose
    private String email;
    @Expose
    private List<Room> ownerRooms;
    @Expose
    private Room currentRoom;

    public UserRequest() {
    }

    public UserRequest(String login, String password, String email, List<Room> ownerRooms, Room currentRoom) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.ownerRooms = ownerRooms;
        this.currentRoom = currentRoom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Room> getOwnerRooms() {
        return ownerRooms;
    }

    public void setOwnerRooms(List<Room> ownerRooms) {
        this.ownerRooms = ownerRooms;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    public boolean validate() {
        return !(login == null
                || password == null
                || email == null
                || ownerRooms == null
                || login.isEmpty()
                || password.isEmpty()
                || email.isEmpty());
    }
}
