package com.celestialapps.roomchats.repository;

import com.celestialapps.roomchats.model.Room;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Sergey on 15.04.2017.
 */
@Transactional
public interface RoomRepository extends CrudRepository<Room, Long> {

    Room findByName(String name);

    List<Room> findByMaxCountUsers(int maxCountUsers);

    @Override
    List<Room> findAll();
}
