package com.celestialapps.roomchats.repository;

import com.celestialapps.roomchats.model.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Sergey on 15.04.2017.
 */
@Transactional
public interface UserRepository extends CrudRepository<User, Long> {

    User findByLogin(String login);
    User findByEmail(String email);

    @Override
    List<User> findAll();
}
