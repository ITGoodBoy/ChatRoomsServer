package com.celestialapps.roomchats.chat;

import com.celestialapps.roomchats.repository.RoomRepository;
import com.celestialapps.roomchats.repository.UserRepository;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by Sergey on 13.04.2017.
 */
@Service
public class ChatServer {

    private final int PORT = 12177;

    private RoomRepository roomRepository;
    private UserRepository userRepository;

    @Autowired
    public ChatServer(RoomRepository roomRepository, UserRepository userRepository) {
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void run() {
        new Thread(() -> {
            EventLoopGroup bossGroup = new NioEventLoopGroup();
            EventLoopGroup workerGroup = new NioEventLoopGroup();

            try {
                ServerBootstrap serverBootstrap = new ServerBootstrap()
                        .group(bossGroup, workerGroup)
                        .channel(NioServerSocketChannel.class)
                        .childHandler(new ChatServerInitializer(roomRepository, userRepository));

                serverBootstrap.bind(PORT).sync().channel().closeFuture().sync();
            }
            catch (InterruptedException e) {}
            finally {
                bossGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
            }
        }).start();
    }

}
