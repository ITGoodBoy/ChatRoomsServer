package com.celestialapps.roomchats.chat;

import com.celestialapps.roomchats.chat.common.Message;
import com.celestialapps.roomchats.chat.common.MessageType;
import com.celestialapps.roomchats.model.Room;
import com.celestialapps.roomchats.model.ServerChatMessage;
import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.repository.RoomRepository;
import com.celestialapps.roomchats.repository.UserRepository;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.concurrent.GlobalEventExecutor;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Sergey on 13.04.2017.
 */

@Service
public class ChatServerHandler extends SimpleChannelInboundHandler<Message> {

    private static RoomRepository roomRepository;
    private static UserRepository userRepository;

    private static final Map<String, ChannelGroup> channelGroups = new ConcurrentHashMap<>();
    private static final Map<Channel, User> usersOnline = new ConcurrentHashMap<>();

    @Autowired
    ChatServerHandler(RoomRepository roomRepository, UserRepository userRepository) {
        ChatServerHandler.roomRepository = roomRepository;
        ChatServerHandler.userRepository = userRepository;
        updateChannelGroups();
    }

    public static void updateChannelGroups() {
        List<Room> roomList = roomRepository.findAll();
        if (roomList == null) return;

        List<String> roomNameList = new ArrayList<>();
        for (Room room : roomList) {
            String roomName = room.getName();
            roomNameList.add(roomName);
            if (!channelGroups.containsKey(roomName))
                channelGroups.put(roomName, new DefaultChannelGroup(GlobalEventExecutor.INSTANCE));
        }

        for (String roomName : channelGroups.keySet()) {
            if (!roomNameList.contains(roomName))
                channelGroups.remove(roomName);
        }
    }


    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
            ctx.channel().writeAndFlush(new Message(MessageType.USER_AUTHORIZE_REQUEST));
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        User user = usersOnline.get(channel);
        removeUserFromRoomCurrentUsers(user.getCurrentRoom().getName(), user.getLogin());
        usersOnline.remove(channel);
        channel.close();
    }

    @Transactional
    private void removeUserFromRoomCurrentUsers(@NotNull String roomName, @NotNull String userLogin) {
        Room room = roomRepository.findByName(roomName);
        if (room == null) return;
        room.getUsersInRoom().remove(userLogin);
        roomRepository.save(room);
    }


    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state() == IdleState.READER_IDLE) {
                ctx.channel().close();
            } else if (e.state() == IdleState.WRITER_IDLE) {
                ctx.writeAndFlush(-1);
            }
        }
    }

    @Override
    protected void channelRead0(@NotNull ChannelHandlerContext channelHandlerContext,
                                @NotNull Message message) throws Exception {

        Channel channel = channelHandlerContext.channel();
        System.out.println(message.getMessageType().toString());

        switch (message.getMessageType()) {
            case USER_AUTHORIZE_RESPONSE:
                addUserToRoom(channel, message.getUser());
                break;
            case TEXT:
                sendMessageAllUsersInRoom(message, channel);
                break;
            case IMAGE:
                sendMessageAllUsersInRoom(message, channel);
                break;
        }
    }

    private void addUserToRoom(@NotNull Channel channel, @NotNull User user) {
        if (authorizeUser(user.getLogin(), user.getPassword())) {
            Room currentRoom = user.getCurrentRoom();
            if (authorizeRoom(currentRoom.getName(), currentRoom.getPassword())) {
                usersOnline.put(channel, user);
                channelGroups.get(currentRoom.getName()).add(channel);
                channel.writeAndFlush(new Message(MessageType.USER_AUTHORIZE_OK));
                saveUserInRoomCurrentUsers(currentRoom.getName(), user.getLogin());
                return;
            }
        }
        channel.writeAndFlush(new Message(MessageType.USER_AUTHORIZE_REQUEST));
    }

    private boolean authorizeUser(@NotNull String userLogin, @NotNull String userPassword) {
        User user = userRepository.findByLogin(userLogin);
        if (user == null) return false;
        if (!userLogin.equals(user.getLogin())) return false;
        if (!userPassword.equals(user.getPassword())) return false;

        return true;
    }


    private boolean authorizeRoom(@NotNull String roomName, @NotNull String roomPassword) {
        Room room = roomRepository.findByName(roomName);
        if (room == null) return false;
        if (!roomName.equals(room.getName())) return false;
        if (room.isEmptyPassword()) return true;
        if (!roomPassword.equals(room.getPassword())) return false;

        return true;
    }

    @Transactional
    private void saveUserInRoomCurrentUsers(@NotNull String roomName, @NotNull String userLogin) {
        Room room = roomRepository.findByName(roomName);
        if (room == null) return;

        room.getUsersInRoom().add(userLogin);
        roomRepository.save(room);
    }

    private void sendMessageAllUsersInRoom(@NotNull Message message, @NotNull Channel channel) {
        if (!usersOnline.containsKey(channel)) return;
        User user = usersOnline.get(channel);

        if (!validateAndRefactorTextMessage(message)) return;
        Room room = user.getCurrentRoom();

        ChannelGroup channelGroup = channelGroups.get(room.getName());
        String date = new SimpleDateFormat("yyyy.MM.dd, HH:mm:ss").format(new Date());

        channelGroup.writeAndFlush(new Message(message.getMessageType(), user.getLogin(), message.getContent(), date));
        boolean isImage = message.getMessageType() == MessageType.IMAGE;

        saveMessageInRoomMessageList(room.getName(), user.getLogin(), isImage, message.getContent(), date);
    }


    private boolean validateAndRefactorTextMessage(@NotNull Message message) {
        if (message.getMessageType() == MessageType.IMAGE) return true;
        if (message.getContent() == null) return false;
        String text = message.getContent();
        if (text.length() > 255) return false;
        if (text.isEmpty()) return false;
        text = text.replaceAll("\\r\\n|\\r|\\n", "");
        message.setContent(text);
        return true;
    }


    @Transactional
    private void saveMessageInRoomMessageList(@NotNull String roomName, @NotNull String login, @NotNull boolean isImage,
                                              @NotNull String textContent, @NotNull String date) {
        Room room = roomRepository.findByName(roomName);
        if (room == null) return;

        ServerChatMessage serverChatMessage = new ServerChatMessage();
        serverChatMessage.setIsImage(isImage);
        serverChatMessage.setDate(date);
        serverChatMessage.setLogin(login);
        serverChatMessage.setContent(textContent);

        room.getMessageList().add(serverChatMessage);
        roomRepository.save(room);
    }


}
