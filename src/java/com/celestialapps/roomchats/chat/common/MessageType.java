package com.celestialapps.roomchats.chat.common;


 public enum MessageType  {
     TEXT,
     IMAGE,
     USER_AUTHORIZE_REQUEST,
     USER_AUTHORIZE_RESPONSE,
     USER_AUTHORIZE_OK
}
