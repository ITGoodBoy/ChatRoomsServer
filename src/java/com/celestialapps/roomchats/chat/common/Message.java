package com.celestialapps.roomchats.chat.common;

import com.celestialapps.roomchats.model.User;
import java.io.Serializable;

public class Message implements Serializable {

    private final MessageType messageType;
    private User user;
    private String login;
    private String content;
    private String date;

    public Message(MessageType messageType) {
        this.messageType = messageType;
    }

    public Message(MessageType messageType, String content) {
        this.messageType = messageType;
        this.content = content;
    }

    public Message(MessageType messageType, String content, String date) {
        this.messageType = messageType;
        this.content = content;
        this.date = date;
    }

    public Message(MessageType messageType, User user) {
        this.messageType = messageType;
        this.user = user;
    }

    public Message(MessageType messageType, User user, String content) {
        this.messageType = messageType;
        this.user = user;
        this.content = content;
    }

    public Message(MessageType messageType, String login, String content, String date) {
        this.messageType = messageType;
        this.login = login;
        this.content = content;
        this.date = date;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
