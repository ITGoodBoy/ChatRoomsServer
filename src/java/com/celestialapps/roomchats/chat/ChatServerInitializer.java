package com.celestialapps.roomchats.chat;

import com.celestialapps.roomchats.chat.common.Message;
import com.celestialapps.roomchats.repository.RoomRepository;
import com.celestialapps.roomchats.repository.UserRepository;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;


/**
 * Created by Sergey on 13.04.2017.
 */
public class ChatServerInitializer extends ChannelInitializer<SocketChannel> {


    private RoomRepository roomRepository;
    private UserRepository userRepository;


    public ChatServerInitializer(RoomRepository roomRepository, UserRepository userRepository) {
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
    }


    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();

        pipeline.addLast(new ObjectEncoder());
        pipeline.addLast(new ObjectDecoder(ClassResolvers.weakCachingResolver(Message.class.getClassLoader())));

        pipeline.addLast(new IdleStateHandler(40, 30, 0, TimeUnit.SECONDS),
                new ChatServerHandler(roomRepository, userRepository));
    }
}
