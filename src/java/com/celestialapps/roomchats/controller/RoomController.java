package com.celestialapps.roomchats.controller;

import com.celestialapps.roomchats.impl.RoomImpl;
import com.celestialapps.roomchats.request.RoomRequest;
import com.celestialapps.roomchats.response.RoomListResponse;
import com.celestialapps.roomchats.response.RoomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Sergey on 15.04.2017.
 */
@RestController
@RequestMapping(value = "rooms/", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomController {

    @Autowired
    private RoomImpl roomService;


    @RequestMapping(value = "get-by-id", method = RequestMethod.GET)
    public RoomResponse getById(long id) {
        return roomService.getById(id);
    }

    @RequestMapping(value = "get-by-name", method = RequestMethod.GET)
    public RoomResponse getByName(String name) {
        return roomService.getByName(name);
    }

    @RequestMapping(value = "get-by-max-count-users", method = RequestMethod.GET)
    public RoomListResponse getByMaxCountUsers(int maxCountUsers) {
        return roomService.getByMaxCountUsers(maxCountUsers);
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public RoomListResponse getAll() {
        return roomService.getAll();
    }

    @RequestMapping(value = "authorize", method = RequestMethod.GET)
    public RoomResponse authorize(String name, String password) {
        return roomService.authorize(name, password);
    }

    @RequestMapping(value = "create", method = RequestMethod.PUT)
    public RoomResponse create(@RequestBody RoomRequest roomRequest) {
        return roomService.create(roomRequest);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public RoomResponse update(@RequestBody RoomRequest roomRequest) {
        return roomService.update(roomRequest);
    }

    @RequestMapping(value = "delete", method = RequestMethod.DELETE)
    public RoomResponse delete(@RequestBody RoomRequest roomRequest) {
        return roomService.delete(roomRequest);
    }
}
