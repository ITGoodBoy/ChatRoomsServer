package com.celestialapps.roomchats.controller;

import com.celestialapps.roomchats.impl.UserImpl;
import com.celestialapps.roomchats.request.UserRequest;
import com.celestialapps.roomchats.response.UserListResponse;
import com.celestialapps.roomchats.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Sergey on 15.04.2017.
 */
@RestController
@RequestMapping(value = "users/", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private UserImpl userService;

    @RequestMapping(value = "get-by-id", method = RequestMethod.GET)
    public UserResponse getById(long id) {
        return userService.getById(id);
    }

    @RequestMapping(value = "get-by-login", method = RequestMethod.GET)
    public UserResponse getByLogin(String login) {
        return userService.getByLogin(login);
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public UserListResponse getAll() {
        return userService.getAll();
    }

    @RequestMapping(value = "authorize", method = RequestMethod.GET)
    public UserResponse authorize(String login, String password) {
        return userService.authorize(login, password);
    }

    @RequestMapping(value = "create", method = RequestMethod.PUT)
    public UserResponse add(@RequestBody UserRequest userRequest) {
        return userService.create(userRequest);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public UserResponse update(@RequestBody UserRequest userRequest) {
        return userService.update(userRequest);
    }

}
