package com.celestialapps.roomchats.service;

import com.celestialapps.roomchats.request.RoomRequest;
import com.celestialapps.roomchats.response.RoomListResponse;
import com.celestialapps.roomchats.response.RoomResponse;

/**
 * Created by Sergey on 15.04.2017.
 */
public interface RoomService {

    RoomResponse authorize(String name, String password);
    RoomResponse getById(long id);
    RoomResponse getByName(String name);
    RoomListResponse getByMaxCountUsers(int maxCountUsers);
    RoomListResponse getAll();
    RoomResponse create(RoomRequest roomRequest);
    RoomResponse update(RoomRequest roomRequest);
    RoomResponse delete(RoomRequest roomRequest);
}
