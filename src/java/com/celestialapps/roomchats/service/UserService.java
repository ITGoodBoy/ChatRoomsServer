package com.celestialapps.roomchats.service;

import com.celestialapps.roomchats.request.UserRequest;
import com.celestialapps.roomchats.response.UserListResponse;
import com.celestialapps.roomchats.response.UserResponse;

import java.util.List;

/**
 * Created by Sergey on 15.04.2017.
 */
public interface UserService {

    UserResponse authorize(String login, String password);
    UserResponse getById(long id);
    UserResponse getByLogin(String login);
    UserListResponse getAll();
    UserResponse create(UserRequest userRequest);
    UserResponse update(UserRequest userRequest);
}
