package com.celestialapps.roomchats.response;

import com.celestialapps.roomchats.model.Room;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Sergey on 01.05.2017.
 */
public class RoomListResponse extends RoomResponse {

    @Expose
    private List<Room> roomList;

    public RoomListResponse(Status status) {
        super(status);
    }

    public RoomListResponse(Status status, List<Room> roomList) {
        super(status);
        this.roomList = roomList;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    @Deprecated
    @Override
    public Room getRoom() {
        throw new UnsupportedOperationException();
    }
}
