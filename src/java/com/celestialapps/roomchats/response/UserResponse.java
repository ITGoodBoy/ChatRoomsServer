package com.celestialapps.roomchats.response;

import com.celestialapps.roomchats.model.User;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Sergey on 30.04.2017.
 */
public class UserResponse {

    @Expose
    private final Status status;
    @Expose
    private final User user;

    public enum Status {
        USER_OK,
        USER_EMPTY_FIELDS,
        USER_ID_NOT_EXIST,
        USER_LOGIN_EXIST,
        USER_LOGIN_NOT_EXIST,
        USER_LOGIN_MIXED_ALPHABET,
        USER_LOGIN_LENGTH_SHORT,
        USER_LOGIN_LENGTH_LONG,
        USER_LOGIN_NUMBER_IN_THE_BEGIN,
        USER_PASSWORD_NOT_EQUALS,
        USER_PASSWORD_LENGTH_SHORT,
        USER_PASSWORD_LENGTH_LONG,
        USER_PASSWORD_EASY,
        USER_EMAIL_EXIST,
        USER_EMAIL_INCORRECT;
     }

    public UserResponse(Status status) {
        this.status = status;
        this.user = null;
    }

     public UserResponse(Status status, User user) {
         this.status = status;
         this.user = user;
     }

    public Status getStatus() {
        return status;
    }

    public User getUser() {
        return user;
    }


}
