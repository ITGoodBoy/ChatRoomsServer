package com.celestialapps.roomchats.model;

import com.celestialapps.roomchats.request.UserRequest;
import com.celestialapps.roomchats.response.UserResponse;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Sergey on 15.04.2017.
 */
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Expose
    private long id;
    @Column(name = "login")
    @Expose
    private String login;
    @Column(name = "password")
    @Expose(serialize = false)
    private String password;
    @Column(name = "email")
    @Expose
    private String email;
    @Column(name = "owner_rooms")
    @OneToMany(targetEntity = Room.class)
    @Expose
    private List<Room> ownerRooms;
    @Column(name = "current_room")
    @Expose
    private Room currentRoom;

    public User() {

    }

    public User(String login, String password, String email, List<Room> ownerRooms, Room currentRoom) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.ownerRooms = ownerRooms;
        this.currentRoom = currentRoom;
    }

    public User(UserRequest userRequest) {
        this.login = userRequest.getLogin();
        this.password = userRequest.getPassword();
        this.email = userRequest.getEmail();
        this.ownerRooms = userRequest.getOwnerRooms();
        this.currentRoom = userRequest.getCurrentRoom();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Room> getOwnerRooms() {
        return ownerRooms;
    }

    public void setOwnerRooms(List<Room> ownerRooms) {
        this.ownerRooms = ownerRooms;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }




}
