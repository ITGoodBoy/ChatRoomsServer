package com.celestialapps.roomchats.model;

import com.celestialapps.roomchats.request.RoomRequest;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Sergey on 15.04.2017.
 */
@Entity
@Table(name = "rooms")
public class Room implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Expose
    private long id;
    @Column(name = "name")
    @Expose
    private String name;
    @Column(name = "password")
    @Expose(serialize = false)
    private String password;
    @Column(name = "is_empty_password")
    @Expose
    private boolean isEmptyPassword;
    @Column(name = "owner", length = 65535)
    @Expose
    private User owner;
    @Column(name = "max_count_users")
    @Expose
    private int maxCountUsers;
    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "users_in_room", joinColumns = @JoinColumn(name = "rooms_id"))
    @OrderColumn
    @Expose
    private List<String> usersInRoom;
    @OneToMany(fetch = FetchType.EAGER, targetEntity = ServerChatMessage.class, cascade = CascadeType.ALL)
    @OrderColumn
    @Expose
    private List<ServerChatMessage> messageList;

    public Room() {
    }

    public Room(String name, String password, boolean isEmptyPassword,
                User owner, int maxCountUsers, List<String> usersInRoom, List<ServerChatMessage> messageList) {
        this.name = name;
        this.password = password;
        this.isEmptyPassword = isEmptyPassword;
        this.owner = owner;
        this.maxCountUsers = maxCountUsers;
        this.usersInRoom = usersInRoom;
        this.messageList = messageList;
    }

    public Room(RoomRequest roomRequest) {
        this.name = roomRequest.getName();
        this.password = roomRequest.getPassword();
        this.isEmptyPassword = roomRequest.isEmptyPassword();
        this.owner = roomRequest.getOwner();
        this.maxCountUsers = roomRequest.getMaxCountUsers();
        this.usersInRoom = roomRequest.getUsersInRoom();
        this.messageList = roomRequest.getMessageList();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public int getMaxCountUsers() {
        return maxCountUsers;
    }

    public void setMaxCountUsers(int maxCountUsers) {
        this.maxCountUsers = maxCountUsers;
    }

    public List<String> getUsersInRoom() {
        return usersInRoom;
    }

    public void setUsersInRoom(List<String> usersInRoom) {
        this.usersInRoom = usersInRoom;
    }

    public List<ServerChatMessage> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<ServerChatMessage> messageList) {
        this.messageList = messageList;
    }

    public boolean isEmptyPassword() {
        return isEmptyPassword;
    }

    public void setEmptyPassword(boolean emptyPassword) {
        isEmptyPassword = emptyPassword;
    }
}
